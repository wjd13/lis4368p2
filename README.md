# LIS4368 - Advanced Web Applications Developmment

## Wyatt Dell

### Project 2 

These requirements complete the JSP/Servlets web application using the MVC framework and providing
create, read, update and delete (CRUD) functionality--that is, the four basic functions of persistent
storage. In addition a search (SCRUD) option could be added, and which will be left as a research
exercise.


### Deliverables:

1. Provide Bitbucket read-only access to p2 repo, *must* include README.md, using Markdown syntax,which displays assignment requirements, screenshots of pre-, post-valid user form entry

2. Blackboard Links: p2 Bitbucket repo

#### Assignment Screenshots:

![Valid User Form Entry](img/valid.png)

![Passed Validation](img/pass.png)

![Modify Form](img/form.png)

![Display Data](img/display.png)

![Modified Data] (img/modified.png)

![Delete Warning](img/delete.png)

![Associated Database Changes](img/db.png)